<?php
require 'includes/dbconnect.inc.php';
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<?php require 'includes/functions.inc.php'; ?>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Survey</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width">
<link rel="stylesheet/less" href="less/style.less">
<script src="js/libs/less-1.3.0.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<?php require 'includes/fontloader.php'; ?>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.2.min.js"><\/script>')</script>
<script src="js/libs/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script>
	 $(document).ready(function() {
		fix_flash(); 
	 });
	</script>
<!-- Use SimpLESS (Win/Linux/Mac) or LESS.app (Mac) to compile your .less files
	to style.css, and replace the 2 lines above by this one:

	<link rel="stylesheet" href="less/style.css">
	 -->

<script src="js/libs/modernizr-2.5.3-respond-1.1.0.min.js"></script>
</head>

<body>
<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<?php require 'menu.php'; ?>
<div class="container">
  <div class="row">
   <div class="span8">
   	<div class="hero-unit about_us_box">
<p>
    <h3 class="umheading">Survey</h3>
</p>
<hr>
These simple and instant surveys are designed  to provide decision-makers with the knowledge and skills they need to be leaders for sustained, integrated institutional effectiveness
<hr>
<h4>
<form method="post" action="includes/survey-evaluate.php">
<?php
$query = "SELECT * from questions";
$query_result = mysql_query($query);
while($question_data = mysql_fetch_assoc($query_result))
			{
				$question_id = $question_data['question_id'];
				$question = $question_data['question_desc'];
				echo '<b>'.$question.'</b><br /><br />';
				$query1 = "SELECT * from options WHERE question_id=$question_id";
				$query_result1 = mysql_query($query1);
				while($option_data = mysql_fetch_assoc($query_result1))
				{
					echo '<label class="radio"><h4><input type="radio" name="'.$option_data['question_id'].'" value="'.$option_data['option_id'].'">'.$option_data['option_desc'].'</h4></label>';
				}
				echo '<hr>';
			}
?>
<br />
 <input type="submit" class="btn btn-lrg"></button>
</form>
</h4>


    </div>
   </div>
   <div class="span4">
    <div class="hero-unit content_box" id="content_box2">
   <p>“ I have seen and worked with Usha in the capacity building of do-good organizations, and I can say that she does it very well.”  </p>
   <div align="right"><b>Willie Cheng</b>, Author, Doing Good Well </div>
   	</div>
   </div>
  </div>
  <hr>
  <footer>
		<div class="row" >
        <div class="span4">
        <b>Developed and maintained by <a href="http://commutatus.com" target="_blank">Commutatus</a></b>
        </div>
          <div align="right">
				<a class="btn" onClick="window.print()"  id="print-btn"><img src="img/print-icon.png" /></a>
				<a class="btn" href="http://www.linkedin.com/pub/usha-menon/14/4ab/144" target="_blank"><img src="img/linkedin.png"></a>
				<a class="btn" href="https://www.facebook.com/UshaMenonManagementConsultancyAsia" target="_blank"><img src="img/facebook.png"></a>
				<a href="https://twitter.com/ushamenon_asia" class="btn" id="twitter-btn" data-show-count="false" data-size="large" target="_blank"><img src="img/tweet.png" /> Follow @ushamenon_asia</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				<span class="st_sharethis_custom btn"><img src="img/share.jpg" /> Share this page</span>
				<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
				<script type="text/javascript">
					stLight.options({
						publisher:'12345',
					});
				</script>
		</div>
        </div>
      </footer>
</div>
<!-- /container -->


<script>
	var _gaq=[['_setAccount','UA-34493045-1'],['_trackPageview']];
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
</body>
</html>